# -*- coding: utf-8 -*-
import numpy as np
import rasterio
from rasterio.features import rasterize
import fiona
import shapely
from shapely.geometry import Polygon, Point, LineString, MultiPolygon, box
import os
import matplotlib.colors


def set_border_mask(rasterize_id):
    """
    Set the border of every geometry to 0.5 in a border_mask fills with 1
    The border is deduce from the rasterize_id array

    Parameters
    ----------
    rasterize_id : np.array
        rasterize geometry according to their id

    Returns
    -------
    np.array
        mask with 0.5 on the border of every geometry

    """
    # pixel into a geometry if all his direct neighbors are also from this geometry
    # neighbor comparison on two axis
    axis1_comp = np.equal(rasterize_id[:-1, 1:-1], rasterize_id[1:, 1:-1])
    axis2_comp = np.equal(rasterize_id[1:-1, :-1], rasterize_id[1:-1, 1:])
    axis1_bool = axis1_comp[1:, :] * axis1_comp[:-1, :]
    axis2_bool = axis2_comp[:, 1:] * axis2_comp[:, :-1]

    mask = np.ones(rasterize_id.shape)
    mask[1:-1, 1:-1] = np.where(axis1_bool * axis2_bool == 1, 0, 1)
    return mask

def create_geom(geom, geomtype):
    """
    Create a shapely geometric object according to the type of the geometry

    Parameters
    ----------
    geom : list or tuple
        Coordinates information of the geometry
    geomtype : str
        Name of the geometry, can be either 'Point', 'LineString' or 'Polygon'

    Returns
    -------
    shapely.geometry.object
        A shapely geometry

    """
    if geomtype == 'Point':
        geomobj = Point(geom)

    elif geomtype == 'Polygon':
        if isinstance(geom[0], list):
            geomobj = Polygon(geom[0])
        else:
            geomobj = Polygon(geom)
            
    elif geomtype == 'LineString':
        geomobj = LineString(geom)

    elif geomtype == 'MultiPolygon':
        # only return first object
        geomobj = MultiPolygon([Polygon(poly[0]) for poly in geom])

    else:
        print(geomtype)
        geomobj = False

    return geomobj

def set_distance_mask(crop_mask, border_mask):
    """
    Set the distance mask for each geometry

    Parameters
    ----------
    crop_mask : np.array
        mask with 1 for field and 0 if not
        
    border_mask : np.array
        mask with 0.5 on the border of every geometry
    Returns
    -------
    np.array
        mask with 0.5 on the border of every geometry

    """
    # initizalize the raster
    distance_mask = np.zeros(crop_mask.shape)
    
    # for each pixel
    for i in range(crop_mask.shape[0]):
        print(i) # to follow the rhythm
        for j in range(crop_mask.shape[1]):
            if crop_mask[i,j]: #if is a crop
                c = 1
                end = False
                while not(end): # goes in diagonal till it found a border
                    for d in range(c):
                        if i-d >= 0 and j-c + d >= 0:
                            if border_mask[i-d, j-c+d]:
                                distance_mask[i,j] = c # update distance
                                end = True
                                break
                        
                        if i + d < crop_mask.shape[0] and j+c - d < crop_mask.shape[1]:
                            if border_mask[i+d, j+c - d]:
                                distance_mask[i,j] = c
                                end = True
                                break
                        
                        if i + d < crop_mask.shape[0] and j-c + d >= 0:
                            if border_mask[i + d, j-c + d]:
                                distance_mask[i,j] = c
                                end = True
                                break
                        
                        if i - d >= 0 and j+c - d < crop_mask.shape[1]:
                            if border_mask[i - d, j+c - d]:
                                distance_mask[i,j] = c
                                end = True
                                break
                        
                    c+=1
                
    return distance_mask
                    
def pixel_rectangle(img, geom):
    """
    Get the pixel box of the geometry inside the image

    Parameters
    ----------
        img : rasterio.open()

        geom : shapely.Polygon
            geometry for which we want the pixel box

    Returns
    -------
        tuple
            tuple of four elements with:
            minimum pixel number of the geom on the column axis,
            minimum pixel number of the geom on the line axis,
            maximum pixel number of the geom on the column axis,
            maximum pixel number of the geom on the line axis

    """
    minx, miny, maxx, maxy = geom.envelope.bounds
    mini, minj, maxi, maxj = *img.index(minx, miny), *img.index(maxx, maxy)
    mini, maxi = min(mini, maxi), max(mini, maxi)
    minj, maxj = min(minj, maxj), max(minj, maxj)
    if mini < 0:
        mini = 0
    if minj < 0:
        minj = 0
    if maxi >= img.shape[0]:
        maxi = img.shape[0] - 1
    if maxj >= img.shape[1]:
        maxj = img.shape[1] - 1
    return mini, minj, maxi, maxj
            

def img_hsv(path, tile, s2_name):
    """
    Get the pixel box of the geometry inside the image

    Parameters
    ----------
        path : str
            path to the global folder that contains 'S2' folder with folder for each tile and then S2 images

        tile : str
            name of the sentinel 2 tile
            
        s2_name : 
            name of the s2 image
    Returns
    -------
        tuple of two np.array
            raster with 4 bands and hsv image
    """
    img = []
    for band in ['B2', 'B3', 'B4', 'B8']:
        img.append(rasterio.open(os.path.join(path, 'S2', tile, S2_name, S2_name + '_FRE_' + band + '.tif')))
    
    raster = np.stack((img[0].read(1)/10000, img[1].read(1)/10000, img[2].read(1)/10000, img[3].read(1)/10000), 2)
    raster[raster > 1] = 1.  #normalize surresponse from the image
    
    #get hsv image
    hsv = matplotlib.colors.rgb_to_hsv(raster[:,:,:3])

    return raster, hsv

def split_data(data, path, prefix, shape_geoms, tile, S2_name):
    rasterinfo = rasterio.open(os.path.join(path, 'S2', tile, S2_name, S2_name + '_FRE_B4.tif'))
    
    for i in range(84): #(10980-256) / 128, espacement de 128pix entre les dalles
        for j in range(84):
            dalle = data[128*i:128*(i+2), 128*j:128*(j+2),:]
            pix_coord = rasterinfo.xy(128*(i+1), 128*(j+1))
            pt = Point(pix_coord[0], pix_coord[1])
            
            for geom in shape_geoms:
                if geom[0].intersects(pt):
                    if geom[1] == 0:
                        with open(os.path.join(path, 'data', 'datatrain', prefix + str(i*84+j) + '.npy'), 'wb') as f:
                            np.save(f, dalle)
                            
                    elif geom[1] == 1:
                        with open(os.path.join(path, 'data', 'datavalid', prefix + str(i*84+j) + '.npy'), 'wb') as f:
                            np.save(f, dalle)
                    
                    break

                
if __name__=="__main__":
    # nom de la tuile
    tile = "30TYQ"
    # path vers le dossier
    path = "/home/ign.fr/hlecomte/Segmentation"
    #image sentinel sur la tuile
    S2_name = "SENTINEL2A_20180623-110520-630_L2A_T"+tile+"_C_V2-2" #"_D_V1-9" 
    date='0623'
    
    ## Create raster from shapefile
    shp_path = os.path.join(path, tile, "rpg_2018_" + tile + "UTM.shp")
    rasterio_path = os.path.join(path, 'S2', tile, S2_name, S2_name + "_ATB_R1.tif")
                               
    path_train_valid = os.path.join(path, 'TrainValid.shp')
    
    #load image et shapefile
    img = rasterio.open(rasterio_path, 'r')
    shapes = fiona.open(shp_path, 'r')
    
    #format shapefile
    shape_geoms_list = [p['geometry'] for p in shapes]
    shape_geoms = [create_geom(p['coordinates'], p['type']) for p in shape_geoms_list if
                   len(p['coordinates'])]
    
    shape_ids = [str(i + 1) for i in range(len(shapes))]
    it = [(i, int(j)) for i, j in zip(shape_geoms, shape_ids)]
    
    # rasterize shapefile sur l'image
    raster_mask = rasterize(it, (img.width, img.height), fill=-1,
                            transform=img.transform)
    
     # create mask
    crop_mask = np.where(raster_mask != -1, 1, 0)
    
    
    border_mask = set_border_mask(raster_mask)
    crop_mask2 = np.where(np.logical_and(crop_mask == 1, border_mask==0), 1, 0)
    
    
    # save crop and border mask
    profile = img.profile
    with rasterio.open(os.path.join(path,  tile, 'rpg_2018_cropmask.tif'), 'w', **profile) as dst:
        dst.write(crop_mask2.astype(rasterio.uint8), 1)
        
    with rasterio.open(os.path.join(path,  tile, 'rpg_2018_bordermask.tif'), 'w', **profile) as dst:
        dst.write(border_mask.astype(rasterio.uint8), 1)
        
    distance_mask = set_distance_mask(crop_mask2, border_mask)
    
    # create norm distance mask
    norm_dist_mask = np.zeros(distance_mask.shape)
    for i in shape_ids:
        mini, minj, maxi, maxj = pixel_rectangle(img, shape_geoms[int(i)-1])
        
        mask_crop = np.where(raster_mask[mini:maxi + 1, minj:maxj + 1] == int(i), 1, 0)
        max_dist = np.max(mask_crop*distance_mask[mini:maxi + 1, minj:maxj + 1])
        
        #test si probleme
        if max_dist == 0:
            max_dist = 1
        
        for u in range(mini, maxi+1):
            for v in range(minj, maxj+1):
                if mask_crop[u-mini, v-minj]:
                    norm_dist_mask[u, v] = distance_mask[u,v]/max_dist
                               
    #set Nan to zero
    norm_dist_mask[np.isnan(norm_dist_mask)] = 0
    
    #change profil pour float and save norm distance mask
    profile.update(dtype=np.float32)
    with rasterio.open(os.path.join(path,  tile, 'rpg_2018_distancenormmask.tif'), 'w', **profile) as dst:
        dst.write(norm_dist_mask.astype(np.float32), 1)
    
    shape_train_valid = fiona.open(path_train_valid, 'r')
    
    shape_geoms = [(create_geom(p['geometry']['coordinates'], p['geometry']['type']), p['properties']['Value']) for p in shape_train_valid]
    
    
    data, hsv = img_hsv(path, tile, S2_name)
    split_data(data, path, 'data'+tile+date, shape_geoms, tile, S2_name)
    data=0
    
    norm_dist_mask = rasterio.open(os.path.join(path,  tile, 'rpg_2018_distancenormmask.tif'))
    border_mask = rasterio.open(os.path.join(path,  tile, 'rpg_2018_bordermask.tif'))
    crop_mask2 = rasterio.open(os.path.join(path,  tile, 'rpg_2018_cropmask.tif'))
    
    
    gt = np.stack((norm_dist_mask.read(1), border_mask.read(1), crop_mask2.read(1), hsv[:,:,0], hsv[:,:,1], hsv[:,:,2]), 2)
    
    split_data(gt, path, 'gt'+tile+date, shape_geoms, tile, S2_name)
    
